﻿using System;

namespace DifficultIntubation
{
	public class NoNewScanResultAvailableException : Exception
	{
		public NoNewScanResultAvailableException ()
		{
		}

		public NoNewScanResultAvailableException(string message) : base(message)
		{
		}
	}
}

