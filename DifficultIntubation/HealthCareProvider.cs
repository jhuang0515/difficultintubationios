﻿using System;

namespace DifficultIntubation
{
	public class HealthCareProvider
	{

		public String Name {
			get;
			set;
		}

		public String Telephone {
			get;
			set;
		}

		public String Email {
			get;
			set;
		}

		public HealthCareProvider () {
			Name = "Not Available";
			Telephone = "Not Available";
			Email = "Not Available";
		}

		public HealthCareProvider (String name, String telephone, String email)
		{
			Name = name;
			Telephone = telephone;
			Email = email;
		}
	}
}

