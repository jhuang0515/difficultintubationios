﻿using System;
using System.Collections.Generic;

namespace DifficultIntubation
{
	public class DifficultIntubationForm
	{
		private static int ENTRY_COUNT = 27;
		private static String MISSING_ENTRY_MESSAGE = "Not Available";

		private String date = MISSING_ENTRY_MESSAGE;

		private String institution = MISSING_ENTRY_MESSAGE;

		private enum Department : int {
			Not_Available = -1,
			Anaesthesia,
			Emergency,
			Intensive_Care,
			Other
		}
		private Department department = Department.Not_Available;

		private HealthCareProvider healthCareProvider = new HealthCareProvider();

		private String indicationForAirwayManagement = MISSING_ENTRY_MESSAGE;

		private enum MallampatiScore : int {
			Not_Available = -1,
			I,
			II,
			III,
			IV
		}
		private MallampatiScore mallampatiScore = MallampatiScore.Not_Available;

		private enum ThyromentalDistanceLessThan6cm : int {
			Not_Available = -1,
			Yes,
			No
		}
		private ThyromentalDistanceLessThan6cm thyromentalDistanceLessThan6cm = ThyromentalDistanceLessThan6cm.Not_Available;

		private enum MouthOpeningLessThan3Fingerbreadths : int {
			Not_Available = -1,
			Yes,
			No
		}
		private MouthOpeningLessThan3Fingerbreadths mouthOpeningLessThan3Fingerbreadths = MouthOpeningLessThan3Fingerbreadths.Not_Available;

		private enum LargeTongue : int {
			Not_Available = -1,
			Yes,
			No
		}
		private LargeTongue largeTongue = LargeTongue.Not_Available;

		private enum NeckMobilityReduced : int {
			Not_Available = -1,
			Yes,
			No
		}
		private NeckMobilityReduced neckMobilityReduced = NeckMobilityReduced.Not_Available;

		private enum RadiationOrSurgeryToHeadOrNeck : int {
			Not_Available = -1,
			Yes,
			No
		}
		private RadiationOrSurgeryToHeadOrNeck radiationOrSurgeryToHeadOrNeck = RadiationOrSurgeryToHeadOrNeck.Not_Available;

		private enum CongenitalAbnormality : int {
			Not_Available = -1,
			Yes,
			No
		}
		private CongenitalAbnormality congenitalAbnormality = CongenitalAbnormality.Not_Available;

		private enum IncreasedAspirationRisk : int {
			Not_Available = -1,
			Yes,
			No
		}
		private IncreasedAspirationRisk increasedAspirationRisk = IncreasedAspirationRisk.Not_Available;

		private enum RaisedBMI : int {
			Not_Available = -1,
			Yes,
			No
		}
		private RaisedBMI raisedBMI = RaisedBMI.Not_Available;

		private enum BagMaskVentilation : int {
			Not_Available = -1,
			Easy,
			With_Oropharyngeal_Airway,
			With_Nasal_Airway,
			Two_Person_Technique,
			Unsuccessful
		}
		private BagMaskVentilation bagMaskVentilation = BagMaskVentilation.Not_Available;

		private Attempt attempt1 = new Attempt();

		private Attempt attempt2 = new Attempt();

		private Attempt attempt3 = new Attempt();

		private String furtherComments = MISSING_ENTRY_MESSAGE;

		public DifficultIntubationForm ()
		{
			try {
				
				List<String> entries = QRCodeScanResultManager.GetEntries ();

				if (entries.Count != ENTRY_COUNT) {
					throw new InsufficientEntriesException ();
				}

				UpdateFieldWithEntry(ref date, entries[0]);
				UpdateFieldWithEntry(ref institution, entries[1]);

				if (IsEnumEntryValid(typeof(Department), entries[2])) {
					department = (Department)GetEnumEntryInteger(entries[2]);
				}

				if (HasEntryBeenFilled(entries[3])) {
					healthCareProvider.Name = entries[3].Trim();
				}

				if (HasEntryBeenFilled(entries[4])) {
					healthCareProvider.Telephone = entries[4].Trim();
				}

				if (HasEntryBeenFilled(entries[5])) {
					healthCareProvider.Email = entries[5].Trim();
				}

				UpdateFieldWithEntry(ref indicationForAirwayManagement, entries[6]);

				if (IsEnumEntryValid(typeof(MallampatiScore), entries[7])) {
					mallampatiScore = (MallampatiScore)GetEnumEntryInteger(entries[7]);
				}

				if (IsEnumEntryValid(typeof(ThyromentalDistanceLessThan6cm), entries[8])) {
					thyromentalDistanceLessThan6cm = (ThyromentalDistanceLessThan6cm)GetEnumEntryInteger(entries[8]);
				}

				if (IsEnumEntryValid(typeof(MouthOpeningLessThan3Fingerbreadths), entries[9])) {
					mouthOpeningLessThan3Fingerbreadths = (MouthOpeningLessThan3Fingerbreadths)GetEnumEntryInteger(entries[9]);
				}

				if (IsEnumEntryValid(typeof(LargeTongue), entries[10])) {
					largeTongue = (LargeTongue)GetEnumEntryInteger(entries[10]);
				}

				if (IsEnumEntryValid(typeof(NeckMobilityReduced), entries[11])) {
					neckMobilityReduced = (NeckMobilityReduced)GetEnumEntryInteger(entries[11]);
				}

				if (IsEnumEntryValid(typeof(RadiationOrSurgeryToHeadOrNeck), entries[12])) {
					radiationOrSurgeryToHeadOrNeck = (RadiationOrSurgeryToHeadOrNeck)GetEnumEntryInteger(entries[12]);
				}

				if (IsEnumEntryValid(typeof(CongenitalAbnormality), entries[13])) {
					congenitalAbnormality = (CongenitalAbnormality)GetEnumEntryInteger(entries[13]);
				}

				if (IsEnumEntryValid(typeof(IncreasedAspirationRisk), entries[14])) {
					increasedAspirationRisk = (IncreasedAspirationRisk)GetEnumEntryInteger(entries[14]);
				}

				if (IsEnumEntryValid(typeof(RaisedBMI), entries[15])) {
					raisedBMI = (RaisedBMI)GetEnumEntryInteger(entries[15]);
				}

				if (IsEnumEntryValid(typeof(BagMaskVentilation), entries[16])) {
					bagMaskVentilation = (BagMaskVentilation)GetEnumEntryInteger(entries[16]);
				}

				UpdateAttempt(ref attempt1, entries[17], entries[18], entries[19]);
				UpdateAttempt(ref attempt2, entries[20], entries[21], entries[22]);
				UpdateAttempt(ref attempt3, entries[23], entries[24], entries[25]);

				UpdateFieldWithEntry(ref furtherComments, entries[26]);
			}
			catch (NoNewScanResultAvailableException exception) {
				throw exception;
			}
		}

		private void UpdateAttempt(ref Attempt attempt, String deviceUsed, String comments, String success) {
			UpdateAttemptDeviceUsed (ref attempt, deviceUsed);
			UpdateAttemptComments (ref attempt, comments);
			UpdateAttemptSuccess (ref attempt, success);
		}

		private void UpdateAttemptDeviceUsed(ref Attempt attempt, String deviceUsed) {
			if (HasEntryBeenFilled (deviceUsed)) {
				attempt.DeviceUsed = deviceUsed.Trim ();
			}
		}

		private void UpdateAttemptComments(ref Attempt attempt, String comments) {
			if (HasEntryBeenFilled (comments)) {
				attempt.Comments = comments.Trim ();
			}
		}

		private void UpdateAttemptSuccess(ref Attempt attempt, String success) {
			if (IsEnumEntryValid (typeof(Attempt.Outcome), success)) {
				attempt.Success = (Attempt.Outcome)GetEnumEntryInteger (success);
			}
		}

		private Boolean IsEnumEntryValid(Type enumType, String entry) {
			try {
				int entryInteger = Convert.ToInt32(entry.Trim());
				return Enum.IsDefined(enumType, entryInteger);
			} catch (Exception exception) {
				return false;
			}
		}

		private int GetEnumEntryInteger(String entry) {
			String trimmedEntry = entry.Trim ();
			return Convert.ToInt32 (trimmedEntry);
		}

		private void UpdateFieldWithEntry(ref String field, String entry) {
			if (HasEntryBeenFilled (entry)) {
				field = entry.Trim();
			}
		}

		private Boolean HasEntryBeenFilled(String entry) {
			return !String.IsNullOrEmpty(entry.Trim ());
		}


		public String GetDescription() {
			return GetLine ("Difficult Intubation Event Details:") +
			GetEmptyLine () +
			GetLine ("Date", date) +
			GetLine ("Institution", institution) +
			GetCleanEnumLine ("Department", department.ToString ()) +
			GetEmptyLine () +
			GetLine ("Contact:") +
			GetEmptyLine () +
			GetLine ("Name", healthCareProvider.Name) +
			GetLine ("Telephone Number", healthCareProvider.Telephone) +
			GetLine ("Email", healthCareProvider.Email) +
			GetEmptyLine () +
			GetLine ("Indication for Airway Management", indicationForAirwayManagement) +
			GetEmptyLine () +
			GetLine ("Pre-OP Physical Characteristics:") +
			GetEmptyLine () +
			GetCleanEnumLine ("Mallampati Score", mallampatiScore.ToString ()) +
			GetCleanEnumLine ("Thyromental Distance < 6 cm", thyromentalDistanceLessThan6cm.ToString ()) +
			GetCleanEnumLine ("Mouth Opening < 3 Fingerbreadths", mouthOpeningLessThan3Fingerbreadths.ToString ()) +
			GetCleanEnumLine ("Large Tongue", largeTongue.ToString ()) +
			GetCleanEnumLine ("Neck Mobility Reduced", neckMobilityReduced.ToString ()) +
			GetCleanEnumLine ("Radiation/Surgery to Head or Neck", radiationOrSurgeryToHeadOrNeck.ToString ()) +
			GetCleanEnumLine ("Congenital Abnormality", congenitalAbnormality.ToString ()) +
			GetCleanEnumLine ("Increased Aspiration Risk", increasedAspirationRisk.ToString ()) +
			GetCleanEnumLine ("Raised BMI", raisedBMI.ToString ()) +
			GetEmptyLine () +
			GetCleanEnumLine ("Bag Mask Ventilation", bagMaskVentilation.ToString ()) +
			GetEmptyLine () +
			GetLine ("Sequence of Events:") +
			GetEmptyLine () +
			GetAttemptDescription ("Attempt 1", attempt1) +
			GetEmptyLine () +
			GetAttemptDescription ("Attempt 2", attempt2) +
			GetEmptyLine () +
			GetAttemptDescription ("Attempt 3", attempt3) +
			GetEmptyLine () +
			GetLine("Further Comments", furtherComments);
		}
			
		private String GetEmptyLine() {
			return "\r";
		}

		private String GetLine(String text) {
			return text + "\r";
		}

		private String GetLine(String label, String value) {
			return label + ": " + value + "\r";
		}

		private String GetCleanEnumString(String enumValue) {
			return enumValue.Replace ('_', ' ');
		}

		private String GetCleanEnumLine(String label, String enumValue) {
			return GetLine (label, GetCleanEnumString (enumValue));
		}

		private String GetAttemptDescription(String label, Attempt attempt) {
			return label + ": \r" +
			GetEmptyLine () +
			GetLine ("Device Used", attempt.DeviceUsed) +
			GetLine ("Comments", attempt.Comments) +
			GetCleanEnumLine ("Successful?", attempt.Success.ToString ());
		}
	}
}

