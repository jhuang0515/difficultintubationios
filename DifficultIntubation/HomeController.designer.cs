// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DifficultIntubation
{
	[Register ("HomeController")]
	partial class HomeController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton ScanQRCodeButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton ViewInformationButton { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ScanQRCodeButton != null) {
				ScanQRCodeButton.Dispose ();
				ScanQRCodeButton = null;
			}
			if (ViewInformationButton != null) {
				ViewInformationButton.Dispose ();
				ViewInformationButton = null;
			}
		}
	}
}
