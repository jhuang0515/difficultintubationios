﻿using System;

namespace DifficultIntubation
{
	public static class QRCodeScanManager
	{
		private static Boolean isNewScanResultAvailable = false;
		private static String newScanResult = String.Empty;

		public static String GetNewScanResult() {
			if (isNewScanResultAvailable) {
				isNewScanResultAvailable = false;
				return newScanResult;
			} else {
				throw new NoNewScanResultAvailableException ();
			}
		}

		public static void UpdateNewScanResult(String result) {
			Console.WriteLine ("Scan result updated with : " + result);
			newScanResult = result;
			isNewScanResultAvailable = true;
		}
	}
}

