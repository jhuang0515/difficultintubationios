using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.IO;
using Plugin.Share;

namespace DifficultIntubation
{
	partial class ViewSavedInformationController : UIViewController
	{
		public ViewSavedInformationController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
			try {
				String difficultIntubationText = FileSavingManager.ReadFile();
				ViewSavedInformationUITextView.Text = difficultIntubationText;
				ExportButton.TouchUpInside += async (object sender, EventArgs e) => {
					await CrossShare.Current.Share(difficultIntubationText, "My Difficult Intubation Information");
				};
				ExportButton.Hidden = false;
			} catch (NoSavedInformationAvailableException exception) {
				ViewSavedInformationUITextView.Text = "No Difficult Intubation information has been saved previously.\r\rPlease first scan the QR Code included in " +
					"the Difficult Intubation form given to you by your healthcare provider.";
				ExportButton.Hidden = true;
			}
		}
	}
}
