// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DifficultIntubation
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton ScanQRCodeButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton ViewMyInformationButton { get; set; }

		[Action ("ViewMyInformationButton_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void ViewMyInformationButton_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (ScanQRCodeButton != null) {
				ScanQRCodeButton.Dispose ();
				ScanQRCodeButton = null;
			}
			if (ViewMyInformationButton != null) {
				ViewMyInformationButton.Dispose ();
				ViewMyInformationButton = null;
			}
		}
	}
}
