﻿using System;

namespace DifficultIntubation
{
	public static class TermsOfServiceManager
	{
		public const String TERMS_OF_SERVICE = 
			"Last updated: July 2nd, 2016\r" +
			"\r" +
			"Please read these Terms of Use (\"Terms\", \"Terms of Use\") carefully before using the Difficult Intubation iOS application (the \"Service\").\r" +
			"\r" +
			"Your access to and use of the Service is conditioned on your acceptance of and compliance of these Terms. These Terms apply to all visitors, users, and others who access or use the Service.\r" +
			"\r" +
			"By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the Terms, then you may not access the Service.\r" +
			"\r" +
			"Terms of Use:\r" +
			"\r"+
			"Before using the Service, you will be asked to indicate your acceptance of the terms and conditions of use as outlined below. By clicking a button marked \"I Accept\" \"I Agree\" \"Okay\" \"I Consent\" or other words and actions that similarly acknowledge your consent or acceptance of a Click Through Agreement, you accept the Terms of Use.\r" +
			"\r" +
			"You represent and warrant that you are at least 18 years of age and that you possess the legal right and ability to be legally bound by these Terms of Use. You agree to be responsible for your use of the Service and to comply with your responsibilities and obligations as stated in these Terms of Use. If you are not 18 years of age, you are not permitted to use this Service and must exit now.\r" +
			"\r" +
			"By using Service, you acknowledge that the Service is only for informational purposes only and is not a replacement for medical advice or medical decision making and the Service is not a substitute for proper communication with patients. Nothing contained on this Service or generated from this Service should be relied upon as a primary source for health or medical information.\r" +
			"\r" +
			"You agree to defend, indemnify, and hold the Service and the owners of the service harmless from any and all claims, liabilities, costs and expenses, including reasonable attorneys' fees, arising in any way from your use of the Service. In no event, including but not limited to negligence, shall the Service or the owners or employees of the Service be liable for any direct, indirect, special, incidental, consequential, exemplary or punitive damages arising from, or directly or indirectly related to, the use of, or the inability to use, the Service and the related functions.\r" +
			"\r" +
			"The information from the Service or generated from the Service should not be considered complete and should not be solely relied upon to suggest a course of treatment. The information generated from the Service is only meant to augment other sources of information and should not be considered a substitute for any or all personal responsibility and diligence. You acknowledge that the Service and the owners of the Service and affiliated entities are not responsible for any false, inaccurate, untrue, unauthorized, or incomplete information submitted by you or any user. The Service and the owners of the Service are not responsible for any errors or inaccuracies during the transfer of any data. The Service and the owners of the Service are also not responsible for any harm, loss, damage, injury, or death associated with any errors or inaccuracies during the transfer of any data.\r" +
			"\r" +
			"The Service and the owners of the Service shall not be responsible for any actual or perceived loss, damage, injury, or death associated with the use of the Service. The Service is not a replacement or substitute for standard medical care or standard medical decision making. The Service and the owners of the Service take no responsibility for your actions or the actions of any medical service providers.\r" +
			"\r" +
			"The Service and the owners of the Service does not store any data entered into the Service. The Service and the owners of the Service are not responsible for any storage of any data entered onto the Service. Any intentional or unintentional storage of data onto any electronic device is the sole responsibility of the user. The Service and the owners of the Service are not responsible for maintaining patient confidentiality or patient privacy; this is the sole responsibility of the user. Patient privacy and patient confidentiality is solely your responsibility and subject to all applicable local, state, provincial, national, and international laws and regulation.\r" +
			"\r" +
			"The Service may contain advertisements of third parties. The inclusion of advertisements on the Service does not imply endorsement of the advertised products or services by the Service or the owners of the Service. The Service and the owners of the Service shall not be responsible for any loss or damage of any kind incurred as a result of the presence of such advertisements on the Service. The Service is not responsible or Liable for any claims, statements, or conduct of any third party advertisers appearing on the Service's website. You shall be solely responsible for any correspondence or transactions you have with any third party advertisers. Your decision to access any links associated with third party advertisements on the Service is entirely at your own risk.\r" +
			"\r" +
			"The Service and its content is the property of its owners. The Service and its content are protected by federal and international copyright and trademark laws. The layout, presentation, an logo are trademarks and any unauthorized use of any trademarks, logos, or any other intellectual property belonging to the owners of the Service is strictly prohibited, and may be prosecuted to the fullest extent of the law.\r" +
			"\r" +
			"Termination\r" +
			"\r" +
			"We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever.\r" +
			"\r" +
			"All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.";
		
		private static Boolean hasUserAccepted = false;

		public static void OnUserAccepted() {
			hasUserAccepted = true;
		}

		public static Boolean HasUserAccepted() {
			return hasUserAccepted;
		}
	}
}

