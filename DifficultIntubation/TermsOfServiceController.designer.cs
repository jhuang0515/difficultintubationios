// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DifficultIntubation
{
	[Register ("TermsOfServiceController")]
	partial class TermsOfServiceController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView TermsOfServiceUITextView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton UserAcceptsButton { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (TermsOfServiceUITextView != null) {
				TermsOfServiceUITextView.Dispose ();
				TermsOfServiceUITextView = null;
			}
			if (UserAcceptsButton != null) {
				UserAcceptsButton.Dispose ();
				UserAcceptsButton = null;
			}
		}
	}
}
