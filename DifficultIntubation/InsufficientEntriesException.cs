﻿using System;

namespace DifficultIntubation
{
	public class InsufficientEntriesException : Exception
	{
		public InsufficientEntriesException ()
		{
		}

		public InsufficientEntriesException(string message) : base(message) 
		{
		}
	}
}

