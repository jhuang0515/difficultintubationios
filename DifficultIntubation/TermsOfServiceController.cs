using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DifficultIntubation
{
	partial class TermsOfServiceController : UIViewController
	{
		public TermsOfServiceController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.

			TermsOfServiceUITextView.Text = TermsOfServiceManager.TERMS_OF_SERVICE;

			UserAcceptsButton.TouchUpInside += (object sender, EventArgs e) => {
				TermsOfServiceManager.OnUserAccepted ();
				StartHomeController ();
			};

			if (TermsOfServiceManager.HasUserAccepted()) {
				StartHomeController ();
			}
		}

		private void StartHomeController() {
			HomeController homeController = this.Storyboard.InstantiateViewController("HomeController") as HomeController;
			if (homeController != null) {
				this.NavigationController.PushViewController(homeController, true);
			}
		}
	}
}
