﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DifficultIntubation
{
	public static class QRCodeScanResultManager
	{
		private static char ENTRY_DELIMITER = '|';

		public static List<String> GetEntries() {
			try {
				List<String> entries = new List<String>();
				char[] QRCodeScanResultCharArray = QRCodeScanManager.GetNewScanResult().ToCharArray();
				StringBuilder entry = new StringBuilder();
				for (int i = 0; i < QRCodeScanResultCharArray.Length; i++) {
					char currentChar = QRCodeScanResultCharArray [i];
					if (currentChar != ENTRY_DELIMITER) {
						entry.Append(currentChar);
					} else {
						entries.Add (entry.ToString ());
						entry.Clear ();
					}
					if (i == QRCodeScanResultCharArray.Length - 1) {
						entries.Add (entry.ToString ());
					}
				}
				return entries;
			} catch (NoNewScanResultAvailableException exception) {
				throw exception;
			}
		}
	}
}

