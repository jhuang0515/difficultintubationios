﻿using System;

namespace DifficultIntubation
{
	public class NoSavedInformationAvailableException : Exception
	{
		public NoSavedInformationAvailableException ()
		{
		}

		public NoSavedInformationAvailableException(string message) : base(message)
		{
		}
	}
}

