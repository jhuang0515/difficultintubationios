using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Collections.Generic;
using System.IO;
using Plugin.Share;

namespace DifficultIntubation
{
	partial class ViewInformationController : UIViewController
	{
		private const String ERROR_MESSAGE = "Empty or invalid QR Code detected!";
		
		public ViewInformationController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
			try {
				DifficultIntubationForm difficultIntubationForm = new DifficultIntubationForm();
				String difficultIntubationText = difficultIntubationForm.GetDescription();
				ViewInformationUITextView.Text = difficultIntubationText;
				FileSavingManager.SaveFile(difficultIntubationText);
				ExportButton.TouchUpInside += async (object sender, EventArgs e) => {
					await CrossShare.Current.Share(difficultIntubationText, "My Difficult Intubation Information");
				};
				ExportButton.Hidden = false;
			} catch (NoNewScanResultAvailableException exception) {
				ViewInformationUITextView.Text = ERROR_MESSAGE;
				ExportButton.Hidden = true;
			} catch (InsufficientEntriesException exception) {
				ViewInformationUITextView.Text = ERROR_MESSAGE;
				ExportButton.Hidden = true;
			}
		}
	}
}
