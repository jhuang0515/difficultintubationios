using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.IO;

namespace DifficultIntubation
{
	partial class HomeController : UIViewController
	{
		public HomeController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.

			ScanQRCodeButton.TouchUpInside += async (object sender, EventArgs e) => {
				var scanner = new ZXing.Mobile.MobileBarcodeScanner();
				var result = await scanner.Scan();
				if (result != null) {
					ViewInformationController viewInformationController = this.Storyboard.InstantiateViewController("ViewInformationController") as ViewInformationController;
					if (viewInformationController != null) {
						QRCodeScanManager.UpdateNewScanResult(result.Text);
						this.NavigationController.PushViewController(viewInformationController, true);
					}
				}
			};

			ViewInformationButton.TouchUpInside += (object sender, EventArgs e) => {
				ViewSavedInformationController viewSavedInformationController = this.Storyboard.InstantiateViewController ("ViewSavedInformationController") as ViewSavedInformationController;
				if (viewSavedInformationController != null) {
					this.NavigationController.PushViewController (viewSavedInformationController, true);
				}
			};
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}
