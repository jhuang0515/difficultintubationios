﻿using System;

namespace DifficultIntubation
{
	public class Attempt
	{
		public String DeviceUsed {
			get;
			set;
		}

		public String Comments {
			get;
			set;
		}

		public enum Outcome {
			Not_Available = -1,
			Success,
			Unsuccessful
		}
		public Outcome Success {
			get;
			set;
		}

		public Attempt ()
		{
			DeviceUsed = "Not Available";
			Comments = "Not Available";
			Success = Outcome.Not_Available;
		}
	}
}

