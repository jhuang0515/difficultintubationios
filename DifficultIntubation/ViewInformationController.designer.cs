// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DifficultIntubation
{
	[Register ("ViewInformationController")]
	partial class ViewInformationController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton ExportButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView ViewInformationUITextView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ExportButton != null) {
				ExportButton.Dispose ();
				ExportButton = null;
			}
			if (ViewInformationUITextView != null) {
				ViewInformationUITextView.Dispose ();
				ViewInformationUITextView = null;
			}
		}
	}
}
