﻿using System;
using System.IO;

namespace DifficultIntubation
{
	public static class FileSavingManager
	{
		private static String FILE_NAME = "My_Information.txt";
		private static String FILE_DIRECTORY = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Difficult_Intubation_App");
		public static String FILE_PATH = Path.Combine(FILE_DIRECTORY, FILE_NAME);

		public static void SaveFile(String text) {
			Directory.CreateDirectory (FILE_DIRECTORY);
			File.WriteAllText (FILE_PATH, text);
		}

		public static String ReadFile() {
			if (File.Exists (FILE_PATH)) {
				return File.ReadAllText (FILE_PATH);
			} else {
				throw new NoSavedInformationAvailableException ();
			}
		}
	}
}

