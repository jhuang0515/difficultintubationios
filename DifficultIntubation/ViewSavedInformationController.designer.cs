// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DifficultIntubation
{
	[Register ("ViewSavedInformationController")]
	partial class ViewSavedInformationController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton ExportButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView ViewSavedInformationUITextView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ExportButton != null) {
				ExportButton.Dispose ();
				ExportButton = null;
			}
			if (ViewSavedInformationUITextView != null) {
				ViewSavedInformationUITextView.Dispose ();
				ViewSavedInformationUITextView = null;
			}
		}
	}
}
